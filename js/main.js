"use strict";

// Теория:
// 1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
//      var - глобальная переменная - к ней есть доступ по всему коду (в этом одна из ее проблем).
//      const - неизменяемая, используется для строгой типизации.
//      let - изменяемая, но видна в своей среде (например в функции, если она в ней, доступ к переменной снаружи функции не получится)

// 2. Почему объявлять переменную через var считается плохим тоном?
//      Как сказал выше, доступна по всему коду. Также, ее можно переписать, иногда этого делать не нужно.

let userName = prompt("Enter your name:");
let userAge = prompt("Enter your age:");

while (
  userName === "" ||
  userName === null ||
  userAge === null ||
  userAge === "" ||
  isNaN(+userAge)
) {
  if (userName === null) {
    userName = "";
  }
  if (userAge === null) {
    userAge = "";
  }
  userName = prompt("Enter your name, again:", userName);
  userAge = prompt("Enter your age, again:", userAge);
  if (
    userName !== "" &&
    userName !== null &&
    userAge !== null &&
    userAge !== "" &&
    !isNaN(+userAge)
  ) {
    break;
  }
}
userAge = +userAge;

if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (userAge <= 22) {
  const answer = confirm("Are you sure you want to continue?");
  console.log(answer);

  if (answer === true) {
    alert("Welcome, " + userName);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert("Welcome, " + userName);
}
